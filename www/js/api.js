angular.module('app.api',['ngResource'])

    .factory('productsApi',['$resource',function ($resource) {
        return $resource('http://cvs.zapto.org/products?page=:id');
    }])
    .factory('searchOptionsApi',['$resource',function ($resource) {
        return $resource('http://cvs.zapto.org/search');
    }])
    .factory('searchApi',['$resource',function ($resource) {
        return $resource('http://cvs.zapto.org/products?brand=:brand&promotion=:promotion&term=:term&categorie=:category&gender=:gender&size=:size&page=:pageId');
    }])
    .factory('orderApi',['$resource',function ($resource) {
        return $resource('http://cvs.zapto.org/quick/store');
    }])
    .factory('wilayasApi',['$resource',function ($resource) {
        return $resource('http://cvs.zapto.org/wilayas');
    }])
    .factory('dairasApi',['$resource',function ($resource) {
        return $resource('http://cvs.zapto.org/wilaya/:id/dairas');
    }])
    .factory('communesApi',['$resource',function ($resource) {
        return $resource('http://cvs.zapto.org/daira/:id/communes')
    }]);