angular.module('app.search.controllers',[])

    .controller('SearchCtrl',[
        '$scope',
        'searchOptionsApi',
        'SearchFactory',
        '$state',
        function ($scope,searchOptionsApi,SearchFactory,$state) {

            $scope.searchOptions = searchOptionsApi.get();
            $scope.searchInfos = {
                "term":"",
                "brand":"",
                "category":"",
                "gender":"",
                "promotion":"",
                "size":[]
            };
            $scope.search = function () {
                SearchFactory.setSearchInfos($scope.searchInfos);
                $state.go('tab.results');
                console.log(SearchFactory.getSearchInfos());
            };
        }])
    .controller('SearchResultCtrl',[
        '$scope',
        'SearchResultFactory',
        'searchApi',
        '$ionicPopup',
        '$timeout',
        '$ionicLoading',
        '$location',
        '$anchorScroll',
        'orderFactory' ,
        '$state',
        'SearchFactory',
        function ($scope,SearchResultFactory,searchApi,$ionicPopup,$timeout,$ionicLoading,$location,$anchorScroll,orderFactory,$state,SearchFactory) {

        $scope.data = {
            productList : [],
            moreData : false,
            oldData:false,
            options:[]
        };



        //Show loading icon
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });



        //init productsList with products in page 1

            searchApi.query(
                {
                    "term":SearchFactory.getSearchInfos().term,
                    "brand":SearchFactory.getSearchInfos().brand,
                    "category":SearchFactory.getSearchInfos().category,
                    "gender":SearchFactory.getSearchInfos().gender,
                    "promotion":SearchFactory.getSearchInfos().promotion,
                    "size[]":SearchFactory.getSearchInfos().size,
                    "pageId":SearchResultFactory.getCurrentPage()
                }
            , function (data) {
                SearchResultFactory.setCurrentPage(data[0].current_page);
                SearchResultFactory.setTotalPages(data[0].last_page);
                SearchResultFactory.setProductsList(data[0].data);
                $scope.data.productList = SearchResultFactory.getProductsList();


                $scope.$broadcast('scroll.infiniteScrollComplete');

                if(SearchResultFactory.getCurrentPage() != SearchResultFactory.getTotalPages()){
                    $scope.data.moreData = true;
                }



                if (SearchResultFactory.getProductsList().length == 0){
                    $scope.data.moreData = false;
                    $ionicPopup.show({
                        title: 'Aucun résultat trouvé !',
                        buttons: [{
                            text: 'Réessayez',
                            onTap: function () {
                                $state.go('tab.search', {}, {'reload': true});
                            }
                        }]
                    });
                }

                $ionicLoading.hide();


            },function (data) {
                $scope.data.moreData = false;
                $ionicLoading.hide();
                $ionicPopup.show({
                    title: 'Erreur de connexion !',
                    buttons: [{
                        text: 'Réessayez',
                        onTap: function () {
                            $state.go('tab.search', {}, {'reload': true});
                        }
                    }]
                });

            });


        $scope.loadMoreData = function () {

            $timeout( function() {

                searchApi.query(
                    {
                        "term":SearchFactory.getSearchInfos().term,
                        "brand":SearchFactory.getSearchInfos().brand,
                        "category":SearchFactory.getSearchInfos().category,
                        "gender":SearchFactory.getSearchInfos().gender,
                        "promotion":SearchFactory.getSearchInfos().promotion,
                        "size[]":SearchFactory.getSearchInfos().size,
                        "pageId":SearchResultFactory.getNextPage()
                    }
                    , function (data) {

                        SearchResultFactory.setCurrentPage(data[0].current_page);
                        SearchResultFactory.setTotalPages(data[0].last_page);
                        SearchResultFactory.setProductsList(data[0].data);
                        $scope.data.productList = SearchResultFactory.getProductsList();
                        $scope.data.oldData = true;

                        if (SearchResultFactory.getCurrentPage() == SearchResultFactory.getTotalPages()) {
                            $scope.data.moreData = false;
                        }

                        //Stop the infinite scroll from spinning
                        $scope.$broadcast('scroll.infiniteScrollComplete');

                        $location.hash('top');
                        $anchorScroll();

                    }, function () {
                        $scope.data.moreData = false;
                    });



            }, 1000);


        };


        $scope.loadOldData = function () {

            $timeout( function() {

                searchApi.query(
                    {
                        "term":SearchFactory.getSearchInfos().term,
                        "brand":SearchFactory.getSearchInfos().brand,
                        "category":SearchFactory.getSearchInfos().category,
                        "gender":SearchFactory.getSearchInfos().gender,
                        "promotion":SearchFactory.getSearchInfos().promotion,
                        "size[]":SearchFactory.getSearchInfos().size,
                        "pageId":SearchResultFactory.getPreviousPage()
                    }
                    , function (data) {

                        SearchResultFactory.setCurrentPage(data[0].current_page);
                        SearchResultFactory.setTotalPages(data[0].last_page);
                        SearchResultFactory.setProductsList(data[0].data);
                        $scope.data.productList = SearchResultFactory.getProductsList();


                        if (SearchResultFactory.getCurrentPage() == 1) {
                            $scope.data.oldData = false;
                        }

                        //Stop the ion-refresher from spinning
                        $scope.$broadcast('scroll.refreshComplete');

                    }, function () {
                        console.log('erreur old data');
                    });



            }, 1000);

        };





        //Popup window shown on button click , shows product options and make an order wish
        $scope.selectedProduct = {
            "size":"",
            "qte":1
        };
        $scope.showPopup = function (product) {

            $scope.data.options = product.options;
            $ionicPopup.show({
                templateUrl: 'templates/popup.html',
                title: product.name,
                subTitle: product.description,
                scope: $scope,
                buttons: [
                    {
                        text: 'Annuler',
                        onTap: function () {

                        }
                    },
                    {
                        text: 'Ajouter',
                        type: 'button-energized',
                        onTap: function () {
                            var order = {
                                name:product.name,
                                id:product.id,
                                price: product.price,
                                promotion:product.promotion,
                                size:$scope.selectedProduct.size,
                                qte:$scope.selectedProduct.qte
                            };
                            orderFactory.addToOrderList(order);
                            $state.go('tab.order');

                        }
                    }]
            });

        };


    }]);