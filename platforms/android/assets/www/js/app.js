angular.module('app',
    [
        'ionic',
        'app.api',
        'app.product',
        'app.order',
        'app.search'
    ])
    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            if(window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if(window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    })
    .config(['$stateProvider', '$urlRouterProvider','$ionicConfigProvider',
        function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {

            $ionicConfigProvider.tabs.position('bottom');
            $ionicConfigProvider.views.maxCache(0);
            $ionicConfigProvider.views.transition('none');
            $ionicConfigProvider.form.checkbox('square');
            $ionicConfigProvider.navBar.alignTitle('center');


            $stateProvider
                .state('root', {
                    url : '/',
                    templateUrl : 'templates/root.html'

                })

                .state('tab', {
                    url : '/tab',
                    templateUrl : 'templates/nav-abstract.html',
                    abstract : true
                })
                .state('tab.home', {
                    url: '/home',
                    views: {
                        'tab': {
                            templateUrl: 'templates/product/product-view.html'
                        }
                    }
                })
                .state('tab.search', {
                    url: '/search',
                    views: {
                        'tab': {
                            templateUrl: 'templates/search/search-view.html'
                        }
                    }
                })
                .state('tab.results', {
                    url: '/results',
                    views: {
                        'tab': {
                            templateUrl: 'templates/search/searchResult-view.html'
                        }
                    }
                })

                .state('tab.order', {
                    url: '/order',
                    views: {
                        'tab': {
                            templateUrl: 'templates/order/order-view.html'
                        }
                    }
                });

            $urlRouterProvider.otherwise('/');
        }])
    .controller('NavController', function($scope, $ionicSideMenuDelegate) {
        // toggle menu to left
        $scope.toggleLeft = function() {
            $ionicSideMenuDelegate.toggleLeft();
        };
    })
    .controller('tabCtrl',[
        '$scope',
        'orderFactory',
        function ($scope,orderFactory) {

            $scope.tabStatus = false;
            if(orderFactory.getOrderList().length ==0 ){
                $scope.tabStatus = true;
            }
        }])
    .controller('rootCtrl',
        function($scope, $state,$ionicHistory,$timeout) {

            // Redirect user to Home page

            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true,
                historyRoot:true
            });
            $timeout(function () {
                $state.go('tab.home');
            }, 2000);


        });


