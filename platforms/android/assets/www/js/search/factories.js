angular.module('app.search.factories',[])

    .factory('SearchFactory',[function () {

        var searchInfos = {};

        return {
            setSearchInfos:function (sinfos) {
                searchInfos = sinfos;

            },
            getSearchInfos:function () {
                return searchInfos;
            }
        };

    }])
    .factory('SearchResultFactory',[function () {
        var currentPage = 1;
        var totalPages = 1;
        var productsList = [];

        return {
            getPreviousPage:function () {
                return currentPage-=1;

            },
            getCurrentPage:function () {
                return currentPage;
            },
            getNextPage:function () {
                if (currentPage!=totalPages){
                    return currentPage+=1;
                }else {
                    return totalPages;
                }
            },
            getTotalPages:function () {
                return totalPages;
            },
            getProductsList:function () {
                return productsList;
            },
            setCurrentPage:function (current) {
                currentPage = current;
            },
            setTotalPages:function (total) {
                totalPages = total;
            },
            setProductsList:function (products) {
                productsList = products;

            }
        };

    }]);