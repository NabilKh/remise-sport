angular.module('app.order.factories',[])

    .factory('orderFactory',[function () {

        var orderList = [];

        return {
            'addToOrderList':function (order) {

                var j =0;
                for (var i=0;i<this.getOrderList().length; i++){
                    if (this.getOrderList()[i].id == order.id && this.getOrderList()[i].size.localeCompare(order.size) ==0){
                        this.getOrderList()[i].qte+=order.qte;
                        break;
                    }
                    j++;
                }
                if (j == this.getOrderList().length || this.getOrderList().length == 0)
                {
                    orderList.push(order);
                }

            },
            'removeItemFromOrderList':function (item) {
                for (var i=0; i<orderList.length; i++) {
                    if(orderList[i].id == item.id){
                        if(orderList[i].qte >1){
                            orderList[i].qte -=1;
                            break;
                        }else {
                            orderList.splice(i,1);
                            break;
                        }
                    }
                }
            },
            'getOrderList':function () {
              return orderList;
            },
            'resetOrderList':function () {
              orderList = [];
            },


            'getTotalPrice':function () {
                var total = 0;
                for (var i = 0; i < orderList.length; i++) {
                    var product = orderList[i];
                    if (product.promotion > 0) {
                        total += (product.price - ((product.price * product.promotion) / 100))* orderList[i].qte;
                    } else {
                        total += product.price* orderList[i].qte;
                    }
                }
                return total;

            }

        };

    }]);