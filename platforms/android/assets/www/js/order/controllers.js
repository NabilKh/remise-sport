angular.module('app.order.controllers',[])

    .controller('OrderCtrl',[
        '$scope',
        'orderFactory',
        '$state',
        'wilayasApi',
        'dairasApi',
        'communesApi',
        '$http',
        '$ionicPopup',
        function ($scope,orderFactory,$state,wilayasApi,dairasApi,communesApi,$http,$ionicPopup) {

            $scope.orderList = orderFactory.getOrderList();

            $scope.data={
                tabStatus : false
            };
            if(orderFactory.getOrderList().length ==0 ){
                $scope.data.tabStatus = true;
            }

            $scope.format = {
                'emailFormat': /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/,
                'telFormat': /^(00213|\+213|\+33|0033|0)(5|6|7|8|9)[0-9]{8}$/
            };


            $scope.clientInfos = {
                "name": "",
                "phone":"",
                "email":"",
                "adress": "",
                "wilaya_id":"",
                "daira_id":"",
                "commune_id":""
            };



            $scope.removeItem = function (item) {
                orderFactory.removeItemFromOrderList(item);
                if(orderFactory.getOrderList().length==0){
                    $state.go('tab.home');
                }
            };

            $scope.total = function(){
                if (!isNaN($scope.wilayascoast)){
                    var livraison = $scope.wilayascoast - (($scope.wilayascoast*$scope.wilayapromotion)/100);
                    return orderFactory.getTotalPrice()+livraison;

                }
                else {
                    return orderFactory.getTotalPrice();
                }

            };

            $scope.wilayas = wilayasApi.query();

            $scope.wilayapromotion = 0;
            $scope.getDairas = function (id,promotion,coast) {
                $scope.clientInfos.wilaya_id = id;
                $scope.wilayapromotion = promotion;
                $scope.wilayascoast = coast;

                $scope.dairas = dairasApi.get({id:id});
            };
            $scope.getCommunes = function (id) {
                $scope.clientInfos.daira_id = id;
                $scope.communes = communesApi.get({id:id});

            };
            $scope.setCommunes = function (id) {
                $scope.clientInfos.commune_id = id;
            };

            $scope.sendOrder = function () {
                var c = {
                    "name":$scope.clientInfos.name,
                    "phone":$scope.clientInfos.phone,
                    "email":$scope.clientInfos.email,
                    "adress":$scope.clientInfos.adress,
                    "wilaya_id":$scope.clientInfos.wilaya_id,
                    "daira_id":$scope.clientInfos.daira_id,
                    "commune_id":$scope.clientInfos.commune_id,
                    "command" : []
                };

                var command = [];

                for (var i =0; i<orderFactory.getOrderList().length;i++){
                    var product = {
                        id:null,
                        size:null,
                        qte:null
                    };
                    product.id = orderFactory.getOrderList()[i].id;
                    product.size = orderFactory.getOrderList()[i].size;
                    product.qte = orderFactory.getOrderList()[i].qte;
                    command.push(product);
                }
                c.command = command;

                $http.post('http://cvs.zapto.org/quick/store',c).then(function () {

                    $ionicPopup.show({
                        title: 'Commande envoyée avec succés !',
                        buttons: [
                            {
                                text: 'OK',
                                type: 'button-energized',
                                onTap: function () {
                                    orderFactory.resetOrderList();
                                    $state.go('tab.home');
                                }
                            }
                        ]
                    });


                }, function () {
                    $ionicPopup.show({
                        title: 'Erreur d\'envoi !',
                        buttons: [
                            {
                                text: 'Réessayez',
                                type:'button-energized',
                                onTap: function () {


                                }
                            }
                        ]
                    });

                });

             };


        }]);