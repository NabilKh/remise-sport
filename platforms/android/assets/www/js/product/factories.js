angular.module('app.product.factories',[])

    .factory('productsFactory',[function () {
        var currentPage = 1;
        var totalPages = 1;
        var productsList = [];

        return {
            getPreviousPage:function () {
                    return currentPage-=1;

            },
            getCurrentPage:function () {
                return currentPage;
            },
            getNextPage:function () {
                if (currentPage!=totalPages){
                    return currentPage+=1;
                }else {
                    return totalPages;
                }
            },
            getTotalPages:function () {
                return totalPages;
            },
            getProductsList:function () {
                return productsList;
            },
            setCurrentPage:function (current) {
                currentPage = current;
            },
            setTotalPages:function (total) {
                totalPages = total;
            },
            setProductsList:function (products) {
                productsList = products;

            }
        };

    }]);