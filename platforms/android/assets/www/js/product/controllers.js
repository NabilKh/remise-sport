angular.module('app.product.controllers',[])

    .controller('ProductCtrl', [
        '$scope',
        'productsApi',
        'productsFactory',
        '$ionicPopup',
        '$timeout',
        '$ionicLoading',
        '$location',
        'orderFactory' ,
        '$state',
        '$anchorScroll',
        function ($scope,productsApi,productsFactory,$ionicPopup,$timeout,$ionicLoading,$location,orderFactory,$state,$anchorScroll) {


            $scope.data = {
                productList : [],
                moreData : false,
                oldData:false,
                options:[]
            };



            //Show loading icon
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });



            //init productsList with products in page 1

            productsApi.query(
                {
                    "id": productsFactory.getCurrentPage()
                }
                , function (data) {
                    productsFactory.setCurrentPage(data[0].current_page);
                    productsFactory.setTotalPages(data[0].last_page);
                    productsFactory.setProductsList(data[0].data);
                    $scope.data.productList = productsFactory.getProductsList();


                    $scope.$broadcast('scroll.infiniteScrollComplete');

                    if(productsFactory.getCurrentPage() != productsFactory.getTotalPages()){
                        $scope.data.moreData = true;
                    }



                    if (productsFactory.getProductsList().length == 0){
                        $scope.data.moreData = false;
                        $ionicPopup.show({
                            title: 'Aucun produit disponible !',
                            buttons: [{
                                text: 'Réessayez',
                                type:'button-energized',
                                onTap: function () {
                                    location.reload(false);
                                }
                            }]
                        });
                    }

                    $ionicLoading.hide();


                },function () {
                    $scope.data.moreData = false;
                    $ionicLoading.hide();
                    $ionicPopup.show({
                        title: 'Erreur de connexion !',
                        buttons: [{
                            text: 'Réessayez',
                            type:'button-energized',
                            onTap: function () {
                                     location.reload(false);
                            }
                        }]
                    });

                });


            $scope.loadMoreData = function () {

                $timeout( function() {

                    productsApi.query(
                        {
                            "id": productsFactory.getNextPage()
                        }
                        , function (data) {

                            productsFactory.setCurrentPage(data[0].current_page);
                            productsFactory.setTotalPages(data[0].last_page);
                            productsFactory.setProductsList(data[0].data);
                            $scope.data.productList = productsFactory.getProductsList();
                            $scope.data.oldData = true;

                            if (productsFactory.getCurrentPage() == productsFactory.getTotalPages()) {
                                $scope.data.moreData = false;
                            }

                            //Stop the infinite scroll from spinning
                            $scope.$broadcast('scroll.infiniteScrollComplete');

                            $location.hash('top');
                            $anchorScroll();

                        }, function () {
                            $scope.data.moreData = false;
                        });



                }, 1000);


            };


            $scope.loadOldData = function () {

                $timeout( function() {

                    productsApi.query(
                        {
                            "id": productsFactory.getPreviousPage()
                        }
                        , function (data) {

                            productsFactory.setCurrentPage(data[0].current_page);
                            productsFactory.setTotalPages(data[0].last_page);
                            productsFactory.setProductsList(data[0].data);
                            $scope.data.productList = productsFactory.getProductsList();


                            if (productsFactory.getCurrentPage() == 1) {
                                $scope.data.oldData = false;
                            }

                            //Stop the ion-refresher from spinning
                            $scope.$broadcast('scroll.refreshComplete');

                        }, function () {
                            console.log('erreur old data');
                        });



                }, 1000);

            };


            //Popup window shown on button click , shows product options and make an order wish
            $scope.selectedProduct = {
                "size":"",
                "qte":1
            };
            $scope.showPopup = function (product) {

                $scope.data.options = product.options;
                $ionicPopup.show({
                    templateUrl: 'templates/popup.html',
                    title: product.name,
                    subTitle: product.description,
                    scope: $scope,
                    buttons: [
                        {
                            text: 'Annuler',
                            onTap: function () {

                            }
                        },
                        {
                            text: 'Ajouter',
                            type: 'button-energized',
                            onTap: function () {
                                var order = {
                                    name:product.name,
                                    id:product.id,
                                    price: product.price,
                                    promotion:product.promotion,
                                    size:$scope.selectedProduct.size,
                                    qte:$scope.selectedProduct.qte
                                };
                                orderFactory.addToOrderList(order);
                                 $state.go('tab.order');

                            }
                        }]
                });

            };



        }]);
